*** Settings ***
Library     RequestsLibrary
Library     BuiltIn
Library     Collections
Library     String
*** Keywords ***
The service should retrive id of 100
    [Arguments]     ${Actual_Result}
                                  # Actual Result here ['Json_Object'] value                  # Expected Result here
    should be equal as strings          ${Actual_Result[99]['id']}                                  100

The service should retrive (userId ,id ,title) objects
    [Arguments]     ${Actual_Result}
                               # Actual Result was retrived from serivce                         # Expected Result here
    log      ${Actual_Result}
    dictionary should contain key        ${Actual_Result[0]}                                               userId
    dictionary should contain key        ${Actual_Result[0]}                                               id
    dictionary should contain key        ${Actual_Result[0]}                                               title

