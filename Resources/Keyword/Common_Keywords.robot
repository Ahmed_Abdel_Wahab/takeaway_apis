*** Settings ***
Resource    ../Objects/API_Preferences.robot

*** Variables ***
${uri}
*** Keywords ***




Create API session
    API_Preferences.API session Preferences



Get the request
    [Documentation]         This keyword to retrive The Get request, it takes two argoment uri and satus code.
    [Arguments]         ${uri}   ${statuscode}
    ${Data}     API_Preferences.Get request as json       ${uri}     ${statuscode}      ${sessionName}
    [Return]  ${Data}


Post data with json
    [Arguments]         ${uri}   ${statuscode}          ${body}
    ${Data}     API_Preferences.Post the request with json        ${uri}      ${sessionName}     ${statuscode}     ${body}
    [Return]  ${Data}



put data with json
    [Arguments]         ${uri}   ${statuscode}
    ${Data}     API_Preferences.put the request with json        ${uri}      ${sessionName}     ${statuscode}
    [Return]  ${Data}

patch data with json
    [Arguments]         ${uri}   ${statuscode}
    ${Data}     API_Preferences.patch the request with json        ${uri}      ${sessionName}     ${statuscode}
    [Return]  ${Data}


delete data
    [Arguments]         ${uri}   ${statuscode}
    ${Data}     API_Preferences.Delete the request        ${uri}      ${sessionName}     ${statuscode}
    [Return]  ${Data}



