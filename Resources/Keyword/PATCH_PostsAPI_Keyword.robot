*** Settings ***
Library     RequestsLibrary
Library     BuiltIn
Library     Collections
Library     String
*** Keywords ***
The service should retrive id of 1
    [Arguments]     ${Actual_Result}
                                  # Actual Result here ['Json_Object'] value                  # Expected Result here
    should be equal as strings          ${Actual_Result['id']}                                  1

The service should retrive (userId, id, title, body) object
    [Arguments]     ${Actual_Result}
                               # Actual Result was retrived from serivce                         # Expected Result here
    dictionary should contain key        ${Actual_Result}                                               userId
    dictionary should contain key        ${Actual_Result}                                               id
    dictionary should contain key        ${Actual_Result}                                               title
    dictionary should contain key        ${Actual_Result}                                               body



