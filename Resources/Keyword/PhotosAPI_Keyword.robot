*** Settings ***
Library     RequestsLibrary
Library     BuiltIn
Library     Collections
Library     String
*** Keywords ***
The service should retrive id of 5000
    [Arguments]     ${Actual_Result}
                                  # Actual Result here ['Json_Object'] value                  # Expected Result here
    should be equal as strings          ${Actual_Result[4999]['id']}                                  5000

The service should retrive (albumId, id, title, url, thumbnailUrl) objects
    [Arguments]     ${Actual_Result}
                               # Actual Result was retrived from serivce                         # Expected Result here
    dictionary should contain key        ${Actual_Result[0]}                                               albumId
    dictionary should contain key        ${Actual_Result[0]}                                               id
    dictionary should contain key        ${Actual_Result[0]}                                               title
    dictionary should contain key        ${Actual_Result[0]}                                               url
    dictionary should contain key        ${Actual_Result[0]}                                               thumbnailUrl

