*** Settings ***
Library     RequestsLibrary
Library     BuiltIn
Library     Collections
Library     String
*** Keywords ***
The service should retrive id of 10
    [Arguments]     ${Actual_Result}
                                  # Actual Result here ['Json_Object'] value                  # Expected Result here
    should be equal as strings          ${Actual_Result[9]['id']}                                  10

The service should retrive (id, name, username, email, ...etc) objects
    [Arguments]     ${Actual_Result}
                               # Actual Result was retrived from serivce                         # Expected Result here
    dictionary should contain key        ${Actual_Result[0]}                                               id
    dictionary should contain key        ${Actual_Result[0]}                                               name
    dictionary should contain key        ${Actual_Result[0]}                                               username
    dictionary should contain key        ${Actual_Result[0]}                                               email
    dictionary should contain key        ${Actual_Result[0]}                                               address

