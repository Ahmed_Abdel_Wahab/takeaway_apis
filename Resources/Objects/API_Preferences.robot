*** Settings ***
Library     RequestsLibrary
Library     BuiltIn
Library     Collections
Library     String
Library     OperatingSystem

*** Variables ***
#### Session Headers Parameters
${Base_Url}                     https://jsonplaceholder.typicode.com
${API_Referer}                  https://jsonplaceholder.typicode.com
${API_Accept_Language}          en-US,en;q=0.5
${API_Accept_content}           text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8
${API_Accept_Encoding}          gzip, deflate, br
${Accept-Language}              en-US,en;q=0.5
${Content-Type}                 application/json
#####################

*** Keywords ***

################## API Preferences ###################
API session Preferences
    Create radom number
    # Below is example of Authorization Header
#    ${headers}=  Create Dictionary          Accept-Language=${Accept-Language}                  Content-Type=${Content-Type}

    # Below is example of API Header
#    ${headers}=  Create Dictionary

    # Below is example of creating session with headers
#    create session      ${sessionName}       ${Base_Url}      ${headers}         timeout=30     disable_warnings=1

    # Below is example of creating session without headers
    create session      ${sessionName}       ${Base_Url}                  timeout=30     disable_warnings=1

Request Headers
    ${Request_headers}=    create dictionary                Content-type=application/json; charset=UTF-8
    set test variable       ${Request_headers}
#####################################################



######### Sart Here HTTP Reqest Method ##########

Get request as json
    [Arguments]  ${uri}        ${statuscode}        ${sessionName}
    Request Headers
    ${response}     get request         ${sessionName}         ${uri}               headers=${Request_headers}
    should be equal as strings      ${response.status_code}         ${statuscode}
    ${data}=    To Json    ${response.content}
    [Return]            ${data}

Get request body from json file as json
#      ${json}=  Evaluate  json.load(open("./Resources/TestData/Posts.json", "r")) | json
     ${json}=   Get file   ./Resources/TestData/Posts.json
     ${json}=   evaluate    json.loads('''${json}''')    json
#     ${port}   Get Json Value  ${json}  /port
#     log      | ${port}
#     ${object}=  Evaluate        json.load(${json})
    [Return]            ${json}
Post the request with json
    [Arguments]  ${uri}     ${sessionName}   ${statuscode}        ${data}
    Request Headers
    ${response}     post request         ${sessionName}         ${uri}                   json=${data}           headers=${Request_headers}
    ${Data}     to json         ${response.content}
    should be equal as strings      ${response.status_code}         ${statuscode}
    [Return]        ${Data}


put the request with json
    [Arguments]  ${uri}     ${sessionName}   ${statuscode}
    Request Headers
    ${response}     put request         ${sessionName}         ${uri}            headers=${Request_headers}
    ${Data}     to json         ${response.content}
    should be equal as strings      ${response.status_code}         ${statuscode}
    [Return]        ${Data}


patch the request with json
    [Arguments]  ${uri}     ${sessionName}   ${statuscode}
    Request Headers
    ${response}     patch request         ${sessionName}         ${uri}                 headers=${Request_headers}
    ${Data}     to json         ${response.content}
    should be equal as strings      ${response.status_code}         ${statuscode}
    [Return]        ${Data}


Delete the request
    [Arguments]  ${uri}     ${sessionName}   ${statuscode}
    Request Headers
    ${response}     delete request          ${sessionName}         ${uri}
    ${Data}     to json         ${response.content}
    should be equal as strings      ${response.status_code}         ${statuscode}
    [Return]        ${Data}

######### End Here HTTP Reqest Method ##############



######## Keyword To Genrate random number ##########

Create radom number
    ${randomNumber}     generate random string          7    [NUMBERS]
    ${sessionName}       Catenate    session_${randomNumber}
    set global variable     ${sessionName}
    set global variable     ${randomNumber}


####################################################


