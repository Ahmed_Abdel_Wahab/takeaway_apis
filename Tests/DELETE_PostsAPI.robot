*** Settings ***
Documentation    Suite description
Resource        ../Resources/Keyword/Common_Keywords.robot
Resource        ../Resources/Keyword/DELETE_PostsAPI_Keyword.robot
*** Variables ***
${Valid_Response}           200
${Created_Response}         201
*** Test Cases ***
Test Case 1
    [Tags]    DELETE /posts/1 API              happy_scenario
    [Documentation]             TC1: Test /posts/1 DELETE API objects count ,response JSON body schema and response code
    ${Actual_Result}         delete data            	/posts/1                  ${Valid_Response}
    The service should retrive empty body          ${Actual_Result}












