*** Settings ***
Documentation    Suite description
Resource        ../Resources/Keyword/Common_Keywords.robot
Resource        ../Resources/Keyword/POST_PostsAPI_Keyword.robot
*** Variables ***
${Valid_Response}           200
${Created_Response}         201
*** Test Cases ***

Test Case 1
    [Tags]    POST /posts API             happy_scenario
    [Documentation]             TC1: Test /posts POST API response value ,response JSON body schema and response code
    Create API session
    ${body}=  Get request body from json file as json
#    ${body}     create dictionary           title=takeaway2       body=time to order food       userId=${randomNumber}
    ${Actual_Result}         Post data with json           	/posts                  ${Created_Response}        ${body}
    The service should retrive id of 1          ${Actual_Result}
    The service should retrive (id) objects         ${Actual_Result}




















