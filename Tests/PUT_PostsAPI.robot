*** Settings ***
Documentation    Suite description
Resource        ../Resources/Keyword/Common_Keywords.robot
Resource        ../Resources/Keyword/PUT_PostsAPI_Keyword.robot
*** Variables ***
${Valid_Response}           200
${Created_Response}         201
*** Test Cases ***
Test Case 1
    [Tags]    PUT /posts/1 API              happy_scenario
    [Documentation]             TC1: Test /posts/1 PUT API objects count ,response JSON body schema and response code
    ${Actual_Result}         put data with json           	/posts/1                  ${Valid_Response}
    The service should retrive id of 1          ${Actual_Result}
    The service should retrive (id) object          ${Actual_Result}












