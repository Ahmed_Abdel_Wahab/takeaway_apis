*** Settings ***
Documentation    Suite description
Resource        ../Resources/Keyword/Common_Keywords.robot
Resource        ../Resources/Keyword/PhotosAPI_Keyword.robot
*** Variables ***
${Valid_Response}           200
${Created_Response}         201
*** Test Cases ***
Test Case 1
    [Tags]    GET /photos API               happy_scenario
    [Documentation]             TC1: Test /photos GET API objects count ,response JSON body schema and response code
    Create API session
    ${Actual_Result}         Get the request            /photos      ${Valid_Response}
    The service should retrive id of 5000          ${Actual_Result}
    The service should retrive (albumId, id, title, url, thumbnailUrl) objects          ${Actual_Result}












