*** Settings ***
Documentation    Suite description
Resource        ../Resources/Keyword/Common_Keywords.robot
Resource        ../Resources/Keyword/PostsAPI_Keyword.robot
*** Variables ***
${Valid_Response}           200
${Created_Response}         201
*** Test Cases ***
Test Case 1
    [Tags]    GET /posts API              happy_scenario
    [Documentation]             TC1: Test /posts GET API objects count ,response JSON body schema and response code
    Create API session
    ${Actual_Result}         Get the request            /posts       ${Valid_Response}
    The service should retrive id of 100          ${Actual_Result}
    The service should retrive (id, title, body, userId) objects          ${Actual_Result}












