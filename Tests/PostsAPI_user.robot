*** Settings ***
Documentation    Suite description
Resource        ../Resources/Keyword/Common_Keywords.robot
Resource        ../Resources/Keyword/PostsAPI_user_Keyword.robot
*** Variables ***
${Valid_Response}           200
${Created_Response}         201
*** Test Cases ***
Test Case 1
    [Tags]    GET /Posts?userId=1 API              happy_scenario
    [Documentation]             TC1: Test /posts?userId=1 GET API objects count ,response JSON body schema and response code
    Create API session
    ${Actual_Result}         Get the request            /posts?userId=1       ${Valid_Response}
    The service should retrive id of 10          ${Actual_Result}
    The service should retrive (id, title, body, userId) objects          ${Actual_Result}












