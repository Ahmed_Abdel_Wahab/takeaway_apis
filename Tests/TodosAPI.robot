*** Settings ***
Documentation    Suite description
Resource        ../Resources/Keyword/Common_Keywords.robot
Resource        ../Resources/Keyword/TodosAPI_Keyword.robot
*** Variables ***
${Valid_Response}           200
${Created_Response}         201
*** Test Cases ***
Test Case 1
    [Tags]    GET /todos API              happy_scenario
    [Documentation]             TC1: Test /Todos GET API objects count ,response JSON body schema and response code
    Create API session
    ${Actual_Result}         Get the request            /todos       ${Valid_Response}
    The service should retrive id of 200          ${Actual_Result}
    The service should retrive (userId, id, title, completed) objects          ${Actual_Result}












