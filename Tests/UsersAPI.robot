*** Settings ***
Documentation    Suite description
Resource        ../Resources/Keyword/Common_Keywords.robot
Resource        ../Resources/Keyword/UsersAPI_Keyword.robot
*** Variables ***
${Valid_Response}           200
${Created_Response}         201
*** Test Cases ***
Test Case 1
    [Tags]    GET /users API              happy_scenario
    [Documentation]             TC1: Test /users GET API objects count and response code
    Create API session
    ${Actual_Result}         Get the request            /users       ${Valid_Response}
    The service should retrive id of 10          ${Actual_Result}
    The service should retrive (id, name, username, email, ...etc) objects          ${Actual_Result}












